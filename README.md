# algo-palindrome


**In Javascript folder**, there is a simple html file that use a javascript function to ckeck if the value given by the input field is a palindrome or not.

Just open index.html in your favorite browser and test the function.

**In Go folder**, there a program that take one argument and check if the passed args is a palindrome.

To test it, just run **go build** the source folder. This command generate a .exe of your .go file.
Then, call your .exe with an extra string argument, like 
> palindrome.exe eye .

The cmd will return **true** or **false**.